pub mod cache;
pub mod database;
pub use util::*;
pub mod bot;
pub mod queries;
pub mod routes;
pub mod structs;
mod util;

use lazy_static::lazy_static;
use once_cell::sync::OnceCell;
use std::time::Instant;

lazy_static! {
    pub static ref START_TIME: OnceCell<Instant> = OnceCell::new();
}
