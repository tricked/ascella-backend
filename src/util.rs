use std::fmt::Display;

use actix_web::{HttpRequest, HttpResponse, ResponseError};
use anyhow::{anyhow, Result};
use http::{HeaderValue, StatusCode};
use lazy_static::lazy_static;
use rand::{distributions::Alphanumeric, prelude::SliceRandom, Rng};
use serde::Serialize;
use serde_json::Value;

use crate::queries::{get_user_token, prelude::Users};
lazy_static! {
    pub static ref CLIENT: reqwest::Client = reqwest::Client::new();
}

#[derive(Serialize, Debug)]
#[serde(tag = "error")]
pub enum Error {
    FileTooLarge { max_size: usize },
    FileTypeNotAllowed,
    FailedToReceive,
    NotAuthorized,
    BlockingError,
    DatabaseError,
    MissingData,
    UnknownTag,
    BadRequest,
    ProbeError,
    NotFound,
    IOError,
    LabelMe,
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, failed)", self.status_code())
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }

    fn description(&self) -> &str {
        "description() is deprecated; use Display"
    }

    fn cause(&self) -> Option<&dyn std::error::Error> {
        self.source()
    }
}

impl ResponseError for Error {
    fn status_code(&self) -> StatusCode {
        match &self {
            Error::FileTooLarge { .. } => StatusCode::PAYLOAD_TOO_LARGE,
            Error::FileTypeNotAllowed => StatusCode::BAD_REQUEST,
            Error::NotAuthorized => StatusCode::FORBIDDEN,
            Error::FailedToReceive => StatusCode::BAD_REQUEST,
            Error::DatabaseError => StatusCode::INTERNAL_SERVER_ERROR,
            Error::MissingData => StatusCode::BAD_REQUEST,
            Error::UnknownTag => StatusCode::BAD_REQUEST,
            Error::ProbeError => StatusCode::INTERNAL_SERVER_ERROR,
            Error::NotFound => StatusCode::NOT_FOUND,
            Error::BlockingError => StatusCode::INTERNAL_SERVER_ERROR,
            Error::IOError => StatusCode::INTERNAL_SERVER_ERROR,
            Error::LabelMe => StatusCode::INTERNAL_SERVER_ERROR,
            Error::BadRequest => StatusCode::BAD_REQUEST,
        }
    }

    fn error_response(&self) -> HttpResponse {
        let body = serde_json::to_string(&self).unwrap();

        HttpResponse::build(self.status_code())
            .content_type("application/json")
            .body(body)
    }
}

const EMOJIS: [char; 92] = [
    '✌', '😂', '😝', '😁', '😱', '👉', '🙌', '🍻', '🔥', '🌈', '☀', '🎈', '🌹', '💄', '🎀', '⚽',
    '🎾', '🏁', '😡', '👿', '🐻', '🐶', '🐬', '🐟', '🍀', '👀', '🚗', '🍎', '💝', '💙', '👌', '❤',
    '😍', '😉', '😓', '😳', '💪', '💩', '🍸', '🔑', '💖', '🌟', '🎉', '🌺', '🎶', '👠', '🏈', '⚾',
    '🏆', '👽', '💀', '🐵', '🐮', '🐩', '🐎', '💣', '👃', '👂', '🍓', '💘', '💜', '👊', '💋', '😘',
    '😜', '😵', '🙏', '👋', '🚽', '💃', '💎', '🚀', '🌙', '🎁', '⛄', '🌊', '⛵', '🏀', '🎱', '💰',
    '👶', '👸', '🐰', '🐷', '🐍', '🐫', '🔫', '👄', '🚲', '🍉', '💛', '💚',
];

fn get_header(val: Option<&HeaderValue>) -> Result<&str> {
    if let Some(val) = val {
        if let Ok(val) = val.to_str() {
            return Ok(val);
        }
    }
    Err(anyhow!(""))
}

pub async fn validate_request(req: &HttpRequest) -> Result<Users, Error> {
    let headers = req.headers();
    let user_id = get_header(headers.get("x-user-id"))
        .unwrap_or("-1")
        .parse::<i32>()
        .unwrap_or(-1);
    let user_token = get_header(headers.get("x-user-token"))
        .unwrap_or_else(|_| get_header(headers.get("x-user-key")).unwrap_or(""));

    if user_id == -1 || user_token.is_empty() {
        Err(Error::NotAuthorized)
    } else if let Ok(user) = get_user_token::exec(user_id, user_token.to_owned()).await {
        log::info!("{} {}", &user.name, &user.id);
        actix_web::rt::spawn(send_text_webhook(format!(
            "**{}** {} {}",
            &req.path(),
            &user.name,
            &user.id
        )));
        Ok(user)
    } else {
        Err(Error::NotAuthorized)
    }
}

fn random_char() -> &'static char {
    EMOJIS.choose(&mut rand::thread_rng()).unwrap()
}

pub fn random_emojis() -> String {
    let mut result = "".to_owned();
    for _i in 1..10 {
        result.push_str(&random_char().to_string());
    }
    result
}

#[cfg(test)]
pub mod test {
    use super::*;
    #[test]
    fn test_emojis() {
        let emojis = random_emojis();
        println!("{}", emojis)
    }
}

pub fn create_config<T: std::fmt::Display, B: std::fmt::Display>(
    user: T,
    password: B,
) -> serde_json::Value {
    serde_json::json!({
      "Version": "13.1.0",
      "Name": "Ascella.host - images",
      "DestinationType": "ImageUploader",
      "RequestMethod": "POST",

      "RequestURL": "https://ascella.wtf/v2/ascella/upload",
      "Body": "MultipartFormData",
      "Headers": {
        "x-user-token": password.to_string(),
        "x-user-id": user.to_string(),
      },
      "FileFormName": "image",
      "URL": "$json:url$",
    })
}

pub fn send_message(code: i32, success: bool, message: &str) -> serde_json::Value {
    serde_json::json!({
      "code": code,
      "success": success,
      "message": message
    })
}

pub fn upload_success(vanity: &str, domain: &str) -> serde_json::Value {
    serde_json::json!({
    "code": 200,
    "success": true,
    "url": format!("{}/{}", domain, vanity),
    "raw": format!("https://ascella.wtf/images/raw/{}", vanity)
    })
}

pub fn ran_str() -> String {
    rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(7)
        .map(char::from)
        .collect()
}

pub async fn send_text_webhook<T: Display>(data: T) -> Result<()> {
    let json = serde_json::json!({ "content": data.to_string() });
    log_shit(json).await?;
    Ok(())
}

pub async fn log_shit(data: Value) -> Result<()> {
    // Set up and send the post request
    CLIENT
        .post(dotenv::var("WEBHOOK").unwrap())
        .header("Content-Type", "application/json")
        .header(
            "User-Agent",
            "discord-webhook-client/0.x Rust client for Discord webhooks",
        )
        .body(data.to_string())
        .send()
        .await?;

    Ok(())
}
