// use crate::database::get_postgres;
// use anyhow::{anyhow, Error, Result};
// use dashmap::DashMap;
// use std::{
//     collections::{BTreeSet, HashSet, VecDeque},
//     fs,
//     hash::Hash,
//     ops::Deref,
//     sync::{Arc, Mutex},
// };

// #[derive(Debug, Default, Clone)]
// pub struct Image {
//     data: Vec<u8>,
//     redirect: Option<String>,
//     owner: i32,
//     id: i32,
//     content_type: String,
// }

// #[derive(Debug, Default)]
// pub struct CacheRef {
//     images: DashMap<String, Image>,
// }

// #[derive(Clone, Debug, Default)]
// pub struct Cache(Arc<CacheRef>);

// impl Cache {
//     pub fn new() -> Self {
//         Self::default()
//     }
//     pub async fn get_image(&mut self, vanity: String) -> Result<Image> {
//         let data = sqlx::query!("SELECT * FROM images WHERE vanity = $1", vanity)
//             .fetch_one(get_postgres().await)
//             .await;
//         if data.is_err() {
//             return Err(anyhow!(""));
//         } else if let Ok(data) = data {
//             let path = format!("./images/{}/{}", data.owner, data.id);
//             let image = &fs::read(path);
//             if image.is_err() {
//                 return Err(anyhow!(""));
//             } else if let Ok(image) = image {
//                 let img = Image {
//                     data: image.to_vec(),
//                     redirect: data.redirect,
//                     owner: data.owner,
//                     id: data.id,
//                     content_type: data.content_type,
//                 };
//                 self.0.images.insert(vanity, img.clone());
//                 return Ok(img);
//             }
//         }

//         return Err(anyhow!(""));
//     }
// }
