use actix_cors::Cors;
use actix_web::{middleware, web, App, HttpServer};
use ascella::{bot::bot::start_bot, routes::v2::*};
// use salvo::Catcher;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    tracing_subscriber::fmt().init();

    tokio::spawn(start_bot());

    HttpServer::new(|| {
        App::new()
            .wrap(
                Cors::default()
                    .allowed_origin_fn(|_, _| true)
                    .allowed_methods(vec!["GET", "POST"])
                    .supports_credentials(),
            )
            .wrap(middleware::Logger::default())
            .service(
                web::scope("/v2")
                    .service(
                        web::scope("/ascella")
                            .service(config::get)
                            .service(domain::post)
                            .service(domains::get)
                            .service(embed::post)
                            .service(public::post)
                            .service(redirect::post)
                            .service(stats::get)
                            .service(upload::post)
                            .service(verify::post)
                            .service(view::get),
                    )
                    .service(
                        web::scope("/paste")
                            .service(paste::get)
                            .service(paste::post),
                    ),
            )
    })
    .bind("0.0.0.0:7878")?
    .run()
    .await
}
